# Metamask Rinkeby how to faucet
https://faucets.chain.link/rinkeby

# Metamask Goerli how to faucet
https://goerlifaucet.com/

# Address I use as owner
0xC6CD41b08DC8f9124933d377431480c69F1e1C9f

# Docus
https://hardhat.org/hardhat-runner/docs/guides/compile-contracts

# Deploy
npx hardhat run --network rinkeby ./scripts/deploy.js
example
https://rinkeby.etherscan.io/address/0x3d964786fae5d4eaF030e4dbEf7f686381eA8982
