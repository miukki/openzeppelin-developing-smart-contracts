
// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.7;

// import ERC 1155 from open zeppelin

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC1155/ERC1155.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/math/SafeMath.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/Counters.sol";

contract NFTContract is ERC1155, Ownable {
    using SafeMath for uint256;

    uint256 public constant PRICE = 1;

    uint256 public constant QTY = 10000;

    // mapping(uint256 => uint256) public tokensRemaining;

    using Counters for Counters.Counter;
    Counters.Counter public _tokenIdCounter;
    
    uint256 public shuffledTokenId = 0;

    mapping (uint256 => string) public _tokenURIs;
    mapping(string => uint8) public hashes;


    // Struct for token
    struct Token {
        address owner;
        uint256 tokenURI;
        uint256 counter;
    }

    mapping (uint => Token) public tokens;

    uint256[] public shuffler = [0];
    uint256 public entropy = block.timestamp;

    //string memory uri_ pass to  ERC1155 look here  https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC1155/ERC1155.sol
    constructor() ERC1155("ipfs://QmRiUxuNZqf74p4nhX2BUE3fUuoNp4nwQuiswN6zJtCP7C/metadata/{id}.json") {
        

        // id 1,2,3
        // _mint(msg.sender, 0, 1, "");
        // _mint(msg.sender, 1, 1, "");
        // _mint(msg.sender, 2, 1, "");
        shuffler = shuffleArray(0, 20000);

        
    }


   function getShuffler() public view returns (uint256[] memory) {
       return shuffler;
   }

   function shuffleArray(uint256 seed, uint256 amount)
        public
        pure
        returns (uint256[] memory)
    {
        uint256[] memory permutations = new uint256[](amount);
        uint256[] memory result = new uint256[](amount);

        uint256 perm;
        uint256 value;
        uint256 index;

        uint256 indexes = amount;

        for (uint256 i = 5; i < amount; i++) {
            seed = uint256(keccak256(abi.encodePacked(seed, i)));
            index = seed % indexes;

            value = permutations[index];
            perm = permutations[indexes - 1];

            result[i] = value == 0 ? index : value - 1;
            permutations[index] = perm == 0 ? indexes : perm;

            indexes--;
        }

        return result;
    }

//mint free
     function mintByID() public  {

        _tokenIdCounter.increment();

        uint256 newItemId = _tokenIdCounter.current();

        require(newItemId >= 0 && newItemId < QTY); 
        
        //add shuffledTokenId

        _mint(msg.sender, newItemId, 1, "");

        tokens[newItemId] = Token(msg.sender, shuffledTokenId, newItemId);



        // require(tokensRemaining[id].sub(amount) >=0);
        // tokensRemaining[id] = tokensRemaining[id].sub(amount);
        //why we can all _mint twice
        //sender an own only one token

    }

//mint with price
     function mintPayable(uint256 id, uint256 amount) public payable {
        require(msg.value >=amount * PRICE * 1 ether); //ether is a shortcut for 10^18)
        // require(tokensRemaining[id].sub(amount) >=0);
        // tokensRemaining[id] = tokensRemaining[id].sub(amount);
        _mint(msg.sender, id, amount, "");
    }

//mint to address by onlyOwner
    function mintByOwner(address account, uint256 id, uint256 amount) public onlyOwner { //onlyOwner si modifier, executed can be only by the owner

         /**
     * @dev Creates `amount` tokens of token type `id`, and assigns them to `to`.
     *
     * Emits a {TransferSingle} event.
     *
     * Requirements:
     *
     * - `to` cannot be the zero address.
     * - If `to` refers to a smart contract, it must implement {IERC1155Receiver-onERC1155Received} and return the
     * acceptance magic value.

    function _mint(
        address to,
        uint256 id,
        uint256 amount,
    )

     */
     
        _mint(account, id, amount, "");
    }
    
    function burn(address account, uint256 id, uint256 amount) public {
        require(msg.sender == account);
        _burn(account, id, amount);
        /**
     * @dev Destroys `amount` tokens of token type `id` from `from`
     *
     * Requirements:
     *
     * - `from` cannot be the zero address.
     * - `from` must have at least `amount` tokens of token type `id`.

    function _burn(
        address from,
        uint256 id,
        uint256 amount
    )

     */
    
    }
    
    

}
