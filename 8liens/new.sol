
// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.7;

// import ERC 1155 from open zeppelin

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract NFTContract is ERC1155, Ownable {
    using SafeMath for uint256;

    uint256 public constant PRICE = 2;

    uint256 public constant QTY = 10000;
    uint256 public constant MAX_SUPPLY_FOR_SHUFFLER = 20001;

    // mapping(uint256 => uint256) public tokensRemaining;

    using Counters for Counters.Counter;
    Counters.Counter public _tokenIdCounter;
    

    mapping (uint256 => string) public _tokenURIs;
    mapping(string => uint8) public hashes;


    // Struct for token
    struct Token {
        address owner;
        uint256 shuffledTokenId;
        uint256 tokenCounter;
    }

    mapping (uint => Token) public tokens;

    uint256[] public shuffle;
    uint256 public entropy = block.timestamp;

    //string memory uri_ pass to  ERC1155 look here  https://github.com/OpenZeppelin/contracts/token/ERC1155/ERC1155.sol
    constructor() ERC1155("ipfs://QmTUquDj5HJvsWF2kL25A8whLGP7Q1JG2rGCMNQ5jFCtyw/{id}.json") {
        

        // id 1,2,3
        // _mint(msg.sender, 0, 1, "");
        // _mint(msg.sender, 1, 1, "");
        // _mint(msg.sender, 2, 1, "");
        // setArrayForShuffle();
        //shuffleArray();
        // shuffle = shuffleFisher(0, 20);
       
    }



    function getShuffler() public view returns (uint256[] memory) {
        return shuffle;
    }

  function shuffleFisher(uint256 seed, uint256 amount) public {
        uint256[] memory permutations = new uint256[](amount);
        uint256[] memory result = new uint256[](amount);

        uint256 perm;
        uint256 value;
        uint256 index;

        uint256 indexes = amount;

        for (uint256 i=5; i < amount; i++) {
            seed = uint256(keccak256(abi.encodePacked(seed, i)));
            index = seed % indexes;

            value = permutations[index];
            perm = permutations[indexes - 1];

            result[i] = value == 0 ? index : value - 1;
            permutations[index] = perm == 0 ? indexes : perm;

            indexes--;
        }

        shuffle = result;
        
    }

//mint free
     function mintByID() public  {

        _tokenIdCounter.increment();

        uint256 newItemId = _tokenIdCounter.current();

        require(newItemId >= 0 && newItemId < QTY); 
        
        uint256 shuffledTokenId = shuffle[newItemId];

        _mint(msg.sender, shuffledTokenId, 1, "");

        tokens[newItemId] = Token(msg.sender, shuffledTokenId, newItemId);

    }


    
}




