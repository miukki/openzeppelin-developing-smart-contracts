// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.7;

contract Shuffle {

    // uint[] public shuffle = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47];
    uint public entropy = block.timestamp;
    uint[] public shuffle;

    function shuffleArray() public {

        for(uint256 i = 0 ; i < shuffle.length; i++) {

            uint256 swapIndex = entropy % (shuffle.length - i);

            uint256 currentIndex = shuffle[i];
            uint256 indexToSwap = shuffle[swapIndex];

            shuffle[i] = indexToSwap;
            shuffle[swapIndex] = currentIndex;
        }
    }
    function push(uint i) public {
        // Append to array
        // This will increase the array length by 1.
        shuffle.push(i);
    }



}