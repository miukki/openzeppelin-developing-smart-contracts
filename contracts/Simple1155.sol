// contracts/GameItems.sol
// SPDX-License-Identifier: MIT
//The most notable aspect is the difference between ERC 1155 and ERC 721 
//focuses largely on support for batch transfers. ERC-1155 allows encompassing 
//multiple assets in one smart contract, thereby enabling their transfer with 
//limited network congestion and lower transaction costs. 
//https://101blockchains.com/erc-1155-vs-erc-721/#:~:text=The%20most%20notable%20aspect%20is,congestion%20and%20lower%20transaction%20costs.

pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract GameItems is ERC1155, Ownable {
    
    using Strings for uint256;
    
    uint256 public constant CHARIZARD = 0;
    uint256 public constant IVYSAUR = 1;
    uint256 public constant VENUSAUR = 2;
    uint256 public constant CHARMANDER = 3;

    uint256[] private ids;
    uint256[] private amomunts;

    address proxyRegistryAddress;
    mapping (uint256 => string) private _uris;

//https://docs.openzeppelin.com/contracts/3.x/api/token/erc1155#ERC1155-_mintBatch-address-uint256---uint256---bytes-
//check ipfs storage nft.storage or car.ipfs.io
    constructor(address _proxyRegistryAddress) ERC1155("https://bafybeihul6zsmbzyrgmjth3ynkmchepyvyhcwecn2yxc57ppqgpvr35zsq.ipfs.dweb.link/{id}.json") {
        _mint(msg.sender, CHARIZARD, 1, "");
        
        ids.push(IVYSAUR);
        ids.push(VENUSAUR);
        ids.push(CHARMANDER);


        amomunts.push(1);
        amomunts.push(1);
        amomunts.push(1);

        proxyRegistryAddress = _proxyRegistryAddress;

        _mintBatch(msg.sender, ids, amomunts, "");
    }
    
    function uri(uint256 tokenId) override public view returns (string memory) {
        return(_uris[tokenId]);
    }
    
    function setTokenUri(uint256 tokenId, string memory uri_updated) public onlyOwner {
        require(bytes(_uris[tokenId]).length == 0, "Cannot set uri twice"); 
        _uris[tokenId] = uri_updated; 
    }

     /**
     * Override isApprovedForAll to whitelist user's OpenSea proxy accounts to enable gas-less listings.
     */
    function isApprovedForAll(address owner, address operator)
        override
        public
        view
        returns (bool)
    {
        // Whitelist OpenSea proxy contract for easy trading.
        ProxyRegistry proxyRegistry = ProxyRegistry(proxyRegistryAddress);
        if (address(proxyRegistry.proxies(owner)) == operator) {
            return true;
        }

        return super.isApprovedForAll(owner, operator);
    }

    
}


/**
  @title An OpenSea delegate proxy contract which we include for whitelisting.
  @author OpenSea
*/
contract OwnableDelegateProxy {}

/**
  @title An OpenSea proxy registry contract which we include for whitelisting.
  @author OpenSea
*/
contract ProxyRegistry {
    mapping(address => OwnableDelegateProxy) public proxies;
}

