// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


contract Raffle {
    address public organizer;
    address[] public players;

     constructor() {

        organizer = msg.sender;

     }


    function enter() public payable {
        require(msg.value > .01 ether);
        players.push(msg.sender);
    }

    function random() private view returns (uint256) {
        return uint256(keccak256(block.difficulty, block.timestamp, players));
    }

    function pickWinner() public {
        uint256 index = random() % players.length;
        players[index].transfer(this.balance);
    }
}