// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";



contract Garden is ERC721 {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIdCounter;
    
    constructor() ERC721("Garden", "GRD") {}

    // Struct for plant
    struct Plant {
        address gardener;
        uint age;
        uint water;
        uint state; // alive, dead, dry
    }
    mapping (uint => Plant) public plants;

    function seed() public {

        _tokenIdCounter.increment();

        uint256 newPlantId = _tokenIdCounter.current();

        _mint(msg.sender, newPlantId);

        plants[newPlantId] = Plant(msg.sender, 0, 0, 0);
    }

    function water(uint plantId) public {
        
        require(plants[plantId].gardener == msg.sender, "Only gardener");

        plants[plantId].water ++;
    }
    
}