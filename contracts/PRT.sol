// contracts/access-control/Auth.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/finance/PaymentSplitter.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract PRT is Ownable, ReentrancyGuard {

    using Strings for uint256;
    

    using Counters for Counters.Counter;
    Counters.Counter public _tokenPRTID_index;


    uint256 public constant PRTID = 20000;
    uint256 public constant MAX_BUYABLE_AMOUNT = 100;
    uint256 public constant MAX_PRT_INDEX = 180000;
    uint256 public constant MAX_SUPPLY_PRT = 160000;

    uint24[] public RTWinnerTikensList;

    uint public constant PRICE_PRT = 0.01 ether;//(uint): number of wei sent with the message

    bool public presalePRT = false;
    bool public luckyMintNFT = false;
    bool public presalepPRTDone = false;

    bytes32 public root;
    

    // Public Raffles Ticket (PRT) : 160,000
    // ID #20,001 to ID #180,000
    mapping(address => uint256[]) userPRTs;
    constructor() {
       
    }

    event DitributePRTs(uint256 indexed PRT_ID, address indexed from); /* This is an event */
    event RTWinnerTokenID(uint date, uint24[] win_list);
    event TransferFromToContract(address from, uint amount);

    // ---modifiers---
    modifier isValidMerkleProof(bytes32[] calldata _proof) {//we need this magic to be sure accounts is holder of PRT
        require(MerkleProof.verify(
            _proof,
            root,
            keccak256(abi.encodePacked(msg.sender))
            ) == true, "Not allowed origin");
        _;
    }

    modifier onlyAccounts () { //for security
        require(msg.sender == tx.origin, "Not allowed origin");
        _;
    }
    
    modifier presalePRTIsDone () {
        require(presalepPRTDone, "Sale PRT is Not Done");
        _;
    }


    modifier presalePRTIsActiveAndNotOver () {
        require(presalePRT, "Sale PRT is Not Open");//needs be true, is active (not paused)
        require(!presalepPRTDone, "Sale PRT is Done"); //is not over
        _;
    }
    // ---modifiers---


    function togglePreSalePRT() public onlyOwner {
        presalePRT = !presalePRT; // we will turn on presale PRT in a day when needs by ourself
    }


    function toggleLuckyMintNFT() public onlyOwner {
        luckyMintNFT = !luckyMintNFT;
    }

    function setMerkleRoot(bytes32 merkleroot)//we need this once all PRT sold 
    onlyOwner 
    public 
    {
        root = merkleroot;
    }

    // ---winnerPRT logic---
    function createXRAND(uint number)  internal pure returns(uint)  {
        //number = 17, 0 - 16 <- this is what we want
        return uint(blockhash(block.number-1)) % number;
    }


    function sendMP() public view onlyAccounts presalePRTIsDone {

        uint xrand = createXRAND(17);

        for (uint i = 0; i < 10000; i++) {
            uint24 _winnerTokenPRTID = uint24(PRTID + 1 + xrand + uint24(16.8888*i));
            RTWinnerTikensList.push(_winnerTokenPRTID);
            emit RTWinnerTokenID(block.timestamp, RTWinnerTikensList);
        }
    }


     function buyPRT (address account, uint8 _amount_wanted_able_to_get) external payable onlyAccounts presalePRTIsActiveAndNotOver nonReentrant {
        require(account != owner(), "Owner of contract can not buy PRT");
        require(msg.sender == account,"Not allowed");
        require(presalePRT, "Sale PRT is Not Open");
        require(_amount_wanted_able_to_get > 0, "Amount buyable needs to be greater than 0");
        require(_amount_wanted_able_to_get <= MAX_BUYABLE_AMOUNT, "You can't mint so much tokens");

        uint weiBalanceWallet = msg.value;
        require(weiBalanceWallet > PRICE_PRT, "Min 0.01 ether");

        uint8 _presaleClaimedAmount = uint8(userPRTs[account].length);
        require(_presaleClaimedAmount >= 0 && _presaleClaimedAmount <= MAX_BUYABLE_AMOUNT, "You have exceeded 100 raffle tickets limit");


        //recalculate amount_wanted if needs
        //uint256 _amount_wanted_able_to_get = amount_wanted; Sam modified
        if (_tokenPRTID_index.current() + PRTID + _amount_wanted_able_to_get > MAX_PRT_INDEX) {  
            _amount_wanted_able_to_get = uint8(MAX_PRT_INDEX - _tokenPRTID_index.current() - PRTID); 
        }
        require(PRICE_PRT * _amount_wanted_able_to_get <= weiBalanceWallet, "Insufficient funds");


        uint256 the_last_index_wanted = _tokenPRTID_index.current() + _amount_wanted_able_to_get + PRTID;
        //require(the_last_index_wanted >= 20001 && the_last_index_wanted <= MAX_PRT_INDEX, "The Last ID PRT token is not correct");    
        require(the_last_index_wanted <= MAX_PRT_INDEX, "The Last ID PRT token is exceeded MAX_PRT_INDEX");    

        //pay first to owner of contract 
        payable(owner()).transfer(PRICE_PRT * _amount_wanted_able_to_get);// bulk send money from sender to owner
        emit TransferFromToContract(msg.sender, PRICE_PRT * _amount_wanted_able_to_get);

        //update userPRTs[account]
        for (uint i = 0; i < _amount_wanted_able_to_get; i++) {
           distributePRTInternal(i, _amount_wanted_able_to_get);
        }

        if (_tokenPRTID_index.current() == MAX_SUPPLY_PRT) {
            presalepPRTDone = true; //toggle presale is done
            sendMP();
        }

     }

    function togglePresalepPRTDone() public onlyOwner {
        presalepPRTDone = !presalepPRTDone; //only owner can toggle presale
    }


    function distributePRTInternal(uint _i, uint _amount) internal nonReentrant {//That means the function has been called only once (within the transaction). 
        _tokenPRTID_index.increment();
        uint256 tokenPRTID = _tokenPRTID_index.current() + PRTID;
        
        //release PRT by update status
        userPRTs[msg.sender].push(tokenPRTID);

        emit DitributePRTs(tokenPRTID, msg.sender);
    }


     function withdraw () public payable onlyOwner {
         payable(msg.sender).transfer(address(this).balance);//This function allows the owner to withdraw from the contract

     }
     

}