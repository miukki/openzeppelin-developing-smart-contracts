// hardhat.config.js
require('@nomiclabs/hardhat-ethers')
const {apiKey, PK, etherscanApiKey} = require('./secrets.json')

/** @type import('hardhat/config').HardhatUserConfig */
const RINKEBY_RPC_URL = `https://rinkeby.infura.io/v3/${apiKey}`
module.exports = {
  solidity: {
    version: '0.8.7',
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
  networks: {
    rinkeby: {
      url: RINKEBY_RPC_URL,
      accounts: [PK],
      gas: 2100000,
      gasPrice: 8000000000,
      saveDeployments: true,
    },
  },
  paths: {
    sources: './contracts',
    tests: './test',
    cache: './cache',
    artifacts: './artifacts'
  },
  etherscan: {
    apiKey: etherscanApiKey
  }
}
